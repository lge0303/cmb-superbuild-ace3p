superbuild_add_project_python(pythonmeshio
  PACKAGE meshio
  DEPENDS pythonsetuptools pythonwheel)

# meshio uses this new `pyproject.toml` setup which doesn't support our use
# case. Just ignore this and use `setup.py`.
superbuild_apply_patch(pythonmeshio setup.py
  "Add a setup.py")
