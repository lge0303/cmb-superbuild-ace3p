string(REPLACE "origin/" "" smtk_tag "${smtk_GIT_TAG}")
string(REPLACE ".git" "/raw/${smtk_tag}/version.txt" smtk_version_url "${smtk_GIT_REPOSITORY}")

# Download SMTK's master branch version.txt
file(DOWNLOAD "${smtk_version_url}"
  ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/smtkVersion.txt STATUS status)
list(GET status 0 error_code)
if (error_code)
  message(FATAL_ERROR "Could not access the version file for SMTK")
endif ()

file(STRINGS ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/smtkVersion.txt version_string )

string(REGEX MATCH "([0-9]+)\\.([0-9]+)\\.([0-9]+)[-]*(.*)"
  version_matches "${version_string}")

set(smtk_version_major ${CMAKE_MATCH_1})
set(smtk_version_minor ${CMAKE_MATCH_2})
set(smtk_version "${smtk_version_major}.${smtk_version_minor}")
if (CMAKE_MATCH_3)
  set(smtk_version_patch ${CMAKE_MATCH_3})
  set(smtk_version "${smtk_version}.${smtk_version_patch}")
else()
  set(smtk_version_patch 0)
endif()
# To be thorough, we should split the label into "-prerelease+metadata"
# and, if prerelease is specified, use it in determining precedence
# according to semantic versioning rules at http://semver.org/ .
# For now, just make the information available as a label:
if (CMAKE_MATCH_4)
  set(smtk_version_label "${CMAKE_MATCH_4}")
endif()
